const connect = require("../db/connect");

async function cleanUpSchedules(){
    const currentDate = new Date();

    //Definir a data para 7 dias atrás
    currentDate.setDate(currentDate.getDate() - 7) 
    //Formata a data para YYYY-MM-DD
    const formattedDate = currentDate.toISOString().split('T')[0];

    const query = `DELETE FROM schedule WHERE dateEnd < ?`
    const values = [formattedDate];

    return new Promise((resolve, reject) => {
        connect.query(query, values, function(err, results){
            if(err){
                console.error("Erro MySql", err);
                return reject(new Error("Erro ao limpar agendamentos"));
            }
            console.log("Agendamentos antigos apagados");
            resolve("Agendamentos limpos com sucesso");
        })
    })
}

module.exports = cleanUpSchedules;