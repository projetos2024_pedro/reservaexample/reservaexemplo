const cron = require("node-cron");
const cleanUpSchedules = require("./services/cleanUpSchedulesServices");

// Agendamento da limpeza
cron.schedule("0 0 * * *", async () => {
    try{
        await cleanUpSchedules();
        console.log("Limpeza automática concluída");
    } catch(error){
        console.error("Erro ao concluir limpeza automática", error);
    }
})