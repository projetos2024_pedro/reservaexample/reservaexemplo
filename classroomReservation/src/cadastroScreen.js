import React, { useState } from "react";
import { View, Button, TextInput, StyleSheet, Text, Alert } from "react-native";
import { useNavigation } from "@react-navigation/native";
import api from "./axios/axios";

const Register = () => {
  const navigation = useNavigation();
  const [credentials, setCredentials] = useState({ email: "", password: "", cpf: "", name: "" });

  const handleRegister = () => {
    // Exemplo de chamada para login
    api.createUser(credentials)
     .then(response => {
      console.log(response.data); // Exibe a resposta da API no console
     Alert.alert("Bem vindo",response.data.message)
    navigation.navigate("HomePage", {
    }); // Vá para a próxima tela após Login
    Alert.alert("Bem vindo")
      })
       .catch(error => {
         Alert.alert("Erro",error.response.data.error);
       });
  };

  return (
    <View style={styles.container}>
        <TextInput
        style={styles.input}
        placeholder="CPF"
        placeholderTextColor="#2C1659"
        onChangeText={(text) => setCredentials({ ...credentials, cpf: text })}
        value={credentials.cpf}
      />
      <TextInput
        style={styles.input}
        placeholder="Name"
        placeholderTextColor="#2C1659"
        onChangeText={(text) => setCredentials({ ...credentials, name: text })}
        value={credentials.name}
      />
      <TextInput
        style={styles.input}
        placeholder="Email"
        placeholderTextColor="#2C1659"
        onChangeText={(text) => setCredentials({ ...credentials, email: text })}
        value={credentials.email}
      />
      <TextInput
        style={styles.input}
        placeholder="Password"
        placeholderTextColor="#2C1659"
        onChangeText={(text) =>
          setCredentials({ ...credentials, password: text })
        }
        value={credentials.password}
        secureTextEntry
      />
      <View style={{height: 10}}/>
      <Button title="Cadastrar" onPress={handleRegister} color="#F266B3"/>
      <View style={{height: 10}}/>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#77ABD9",
  },
  input: {
    width: "80%",
    marginBottom: 10,
    padding: 15,
    color: "#2C1659",
    borderWidth: 1,
    borderColor: "#2C1659",
    borderRadius: 10,
  },
});

export default Register;
