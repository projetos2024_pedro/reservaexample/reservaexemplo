import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import HomeScreen from "./src/loginScreen";
import HomePage from "./src/homePage";
import Register from "./src/cadastroScreen";


const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{ title: "Login",
            headerShown: false
           }}
        />
        <Stack.Screen
          name="Register"
          component={Register}
          options={{ title: "Register",
            headerShown: false
           }}
        />
        <Stack.Screen
          name="HomePage"
          component={HomePage}
          options={{ title: "Classroom Reservation", headerShown: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
